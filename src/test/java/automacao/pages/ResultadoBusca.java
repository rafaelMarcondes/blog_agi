package automacao.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class ResultadoBusca {
	
	public static final String TEXT_RESULTADO_BUSCA = "/html/body/div[2]/div/div/header/h1";
	
	public static String getValorResultadoBusca(WebDriver browser) {
		return browser.findElement(By.xpath(TEXT_RESULTADO_BUSCA)).getText();
	}

}
