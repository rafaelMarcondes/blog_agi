package automacao.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class HomePage {

	private static final String ID_LUPA_BUSCA = "search-open";
	private static final String XP_CAMPO_PESQUISAR = "/html/body/header/div[1]/div[2]/form/label/input";
	private static final String XP_BOTAO_PESQUISAR = "/html/body/header/div[1]/div[2]/form/input";
	private static final int um_segundo = 1;
	
	public static void clicaNalupa(WebDriver browser) {
		browser.findElement(By.id(HomePage.ID_LUPA_BUSCA)).click();
	}
	
	public static boolean campoDePesquisaEstaAparente(WebDriver browser) {
		return new WebDriverWait(browser, um_segundo)
		.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(XP_CAMPO_PESQUISAR))).isDisplayed();
	}
	
	public static boolean campoDePesquisaNaoEstaAparente(WebDriver browser) {
		return new WebDriverWait(browser, um_segundo)
				.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(XP_CAMPO_PESQUISAR)));
	}
	
	public static void digitaNoCampoPesquisar(WebDriver browser, String valor) {
		new WebDriverWait(browser, um_segundo)
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(XP_CAMPO_PESQUISAR))).sendKeys(valor);
	}
	
	public static void clicaNoBotaoPesquisar(WebDriver browser) {
		browser.findElement(By.xpath(XP_BOTAO_PESQUISAR)).click();
	}
	
}
