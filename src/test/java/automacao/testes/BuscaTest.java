package automacao.testes;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import automacao.config.Configuracoes;
import automacao.pages.HomePage;
import automacao.pages.ResultadoBusca;

public class BuscaTest extends Configuracoes{
  
		@BeforeClass
		public static void inicio() {
			abreBrowser();
		}
		
		@AfterClass
		public static void fim() {
			encerra();
		}
		
		@After
		public void fimDoTeste() {
			navegaAteHome();
		}
		
		@Test
		public void testCampoVisivelAposCliqueNaLupa(){
			HomePage.clicaNalupa(browser);
			assertEquals(true, HomePage.campoDePesquisaEstaAparente(browser));
		}
		
		@Test
		public void testCampoInvisivelAposCliqueNaLupa(){
			HomePage.clicaNalupa(browser);
			HomePage.clicaNalupa(browser);
			assertEquals(true, HomePage.campoDePesquisaNaoEstaAparente(browser));
		}
		
		@Test
		public void testBuscaValorValido(){
			final var busca = "Cartão de crédito";
			HomePage.clicaNalupa(browser);
			HomePage.digitaNoCampoPesquisar(browser, busca);
			HomePage.clicaNoBotaoPesquisar(browser);
			assertEquals("Resultados da busca por: " + busca, ResultadoBusca.getValorResultadoBusca(browser));
			
		}
		
		
		
}
