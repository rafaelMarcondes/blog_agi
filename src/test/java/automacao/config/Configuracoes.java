package automacao.config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Configuracoes {
	
	private static final String URL = "http://blogdoagi.com.br"; 
	public static WebDriver browser;
	
	public static WebDriver abreBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\desenvolvimento\\chormedriver\\chromedriver.exe");
		browser = new ChromeDriver();
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		browser.manage().window().maximize();
		navegaAteHome();
		return browser;
	}
	
	public static void navegaAteHome() {
		browser.navigate().to(URL);
	}
	
	public static void encerra() {
		browser.close();
		browser.quit();
	}
	
}
